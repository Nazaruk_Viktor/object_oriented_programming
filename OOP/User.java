
public class User {
	String login;
	String password;
	Basket basket;
	public User(String login, String password, Basket basket){
		this.login = login;
		this.password = password;
		this.basket = basket;
	}
	public String Full(){
		return "User - " + login + "\n" + "Password - hidden\n" + "List of purchased goods:" + basket.Return();
	}
}
