
public class Basket {
	Product[] purchased_goods;
	public Basket(Product[] purchasedGoods){
		this.purchased_goods = purchasedGoods;
	}
	public String Return(){
		StringBuilder res = new StringBuilder();
		for (Product purchased_good : purchased_goods) {
			res.append(purchased_good.Returns());
		}
		return res.toString();
	}
}
