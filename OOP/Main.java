public class Main{
	public static void main(String[] args){
		Category category_vegetables = new Category("Vegetables", new Product[]{new Product("Tomato", 70, 4), new Product("Cucumber", 30, 4.2)});
		Category category_fruit = new Category("Fruit",  new Product[]{new Product("Apple", 32, 4.5), new Product("Banana", 120, 4.5)});
		Category category_grocery = new Category("Seafood",  new Product[]{new Product("Salmon", 4000, 4.8), new Product("Shrimps", 500, 5)});
		System.out.println("Categories and Products:\n");
		category_vegetables.Full();
		category_fruit.Full();
		category_grocery.Full();
		User user = new User("Alex", "12345678", new Basket(new Product[]{new Product("Tomato", 70, 4), new Product("Banana", 120, 4.5)}));
		System.out.println(user.Full());
    }
}




