
public class Product {
	public String name;
	public int price;
	public double rating;
	public Product(String name, int price, double rating){
		this.name = name;
		this.price = price;
		this.rating = rating;
	}
	public String Returns(){
		return "\n" + name + " " + price + " rub/kg" + " " + rating + "/5.0";
	}
}
